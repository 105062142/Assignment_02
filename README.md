# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
### 1. Acomplete game process:

* start menu  

<img src = "readme_img/menu.png" width = "600px" height = "400px"></img>
* game view   

 <img src = "readme_img/game.png" width = "600px" height = "400px"></img>
* game over  

 <img src = "readme_img/gameover.png" width = "600px" height = "400px"></img>
* quit or play again

 <img src = "readme_img/quit.png" width = "600px" height = "400px"></img>

### 2. follow the basic rules of “小朋友下樓梯”
 向小朋友下樓梯一樣，1P透過左右鍵，2P透過AD鍵，來控制方向，在遊戲空間內踩不斷上刷的平台，且須盡量不碰到頂端的尖刺以及隨機產生的刺刺平台，而不落下。
### 3. interesting traps
 除了正常的平台之外，另外有五種特殊平台：刺刺平台、假的平台、向右以及向左平台、彈簧平台，刺刺平台碰到會損失三滴血，假的平台踩上去後會翻掉使玩家掉下去，向左向右平台則會將玩家往該方向推，彈簧平台會讓玩家跳上去，一不注意會容易碰到頂端尖刺而損失五滴血。
### 4. physical properties
 玩家只能在有效空間移動,若碰到左右邊的牆壁和上端尖刺,會碰撞而過不去。而落下時也會有重力。
### 5. additionalsound effects
 踩上每個不同的平台會有不同音效，掉下去會尖叫，按下選項也會有音效。
### 6. leaderboard
 在死掉的時候可以選擇儲存自己的名字，若有在前五名，可以在一開始的menu按下S進入排行榜，這時候就可以在上方看到自己的名字、分數和排名。
### 7. Creative features 
* 可以兩個人同時玩

<img src = "readme_img/two_players.png" width = "600px" height = "400px"></img>

* 可以在畫面右手邊使用pause鍵來暫停、abort鍵來回到選單(滑鼠點擊)

<img src = "readme_img/pause.png" width = "600px" height = "400px"></img>
 
* 排行榜使用bitmaptext做出個人化字體

<img src = "readme_img/scoreboard.png" width = "600px" height = "400px"></img>

* 增加難度  
每10階(對應於時間的25秒),會在製造platform的group多新增3個刺刺平台,以增加刺刺平台出現機率