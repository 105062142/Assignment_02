var loadState = {
    preload : function()
    {
        var loadingLabel = game.add.text(game.width / 2,170,'loading...',{font : '30px Ariel',fill : '#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);

        var bottom = game.add.sprite(game.width / 2,200,'loading_bar');
        bottom.anchor.setTo(0.5,0.5);
        var progressBar = game.add.sprite(game.width / 2 - 47,200,'load');
        progressBar.anchor.setTo(0,0.5);
        game.load.setPreloadSprite(progressBar);

        //image
        game.load.spritesheet('player', 'img/player.png', 32, 32); // 主角 
        game.load.spritesheet('player2','img/player2.png',32,32);
    	game.load.image('wall', 'img/wall.png'); // 牆壁
    	game.load.image('ceiling', 'img/ceiling.png'); // 天花板的刺 
    	game.load.image('normal', 'img/normal.png'); // 藍色平台
    	game.load.image('nails', 'img/nails.png'); // 帶刺平台
    	game.load.image('bg','img/background.png'); //背景
        game.load.image('border','img/border.png'); //外邊框
        game.load.spritesheet('life','img/life.png',96,16); // 生命條
        game.load.image('life_title','img/life_title.png'); // 生命值標題
        game.load.image('life_title_two','img/score2.png');
    	game.load.spritesheet('conveyorRight', 'img/conveyor_right.png', 96, 16); // 向右捲動的平台
    	game.load.spritesheet('conveyorLeft', 'img/conveyor_left.png', 96, 16); // 向左捲動的平台
    	game.load.spritesheet('trampoline', 'img/trampoline.png', 96, 22); // 彈簧墊
        game.load.spritesheet('fake', 'img/fake.png', 96, 36); // 翻轉的大平台
        game.load.spritesheet('number','img/number.png',31,32); //分數
        game.load.image('title','img/title.png'); //首頁標題
        game.load.image('enter','img/enter.png'); //enter
        game.load.image('s','img/s.png');
        game.load.image('esc','img/esc.png');
        game.load.image('enter_record','img/enter_record.png');
        game.load.image('scoreboard','img/scoreboard.png');
        game.load.image('name','img/name.png');
        game.load.image('score','img/score.png');
        game.load.image('gameover','img/gameover.png');
        game.load.image('enter_name','img/enter_name.png');
        game.load.image('esc_restart','img/esc_restart.png');
        game.load.image('down','img/down.png');
        game.load.image('up','img/up.png');
        game.load.image('down_record','img/down_record.png');
        game.load.image('up_record','img/up_record.png');
        game.load.image('r','img/r.png');
        game.load.image('1p','img/1p.png');
        game.load.image('1p_rule_left','img/rule_left.png');
        game.load.image('1p_rule_right','img/rule_right.png');
        game.load.image('2p','img/2p.png');
        game.load.image('2p_rule_left','img/2p_rule_left.png');
        game.load.image('2p_rule_right','img/2p_rule_right.png');
        game.load.image('rule','img/rule.png');
        game.load.image('gameover_0','img/gameover_0.png');
        game.load.image('player1win','img/player1win.png');
        game.load.image('player2win','img/player2win.png');
        game.load.image('tie','img/tie.png');
        game.load.image('crown','img/crown.png');
        game.load.image('2','img/2.png');
        game.load.image('3','img/3.png');
        game.load.image('4','img/4.png');
        game.load.image('5','img/5.png');
        game.load.spritesheet('record_score','img/small_font.png',13,14);
        game.load.image('pause_button','img/pause_button.png');
        game.load.image('pause_label','img/pause_label.png');
        game.load.image('abort_button','img/abort_image.png');
        game.load.image('english_label','img/english.png');

        //audio
        game.load.audio('normalsound','audio/normal.wav');
        game.load.audio('fakesound','audio/fake.wav');
        game.load.audio('jumpsound','audio/jump.wav');
        game.load.audio('nailsound','audio/nails.wav');
        game.load.audio('diesound','audio/die.wav');
        game.load.audio('conveyorsound','audio/conveyor.wav');
        game.load.audio('inst','audio/inst.wav');

    },

    create : function()
    {
        game.state.start('menu');
    }
};