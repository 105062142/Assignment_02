var playersState = {

    preload: function() {},

    create: function() 
    {
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.global.score = 1;
        game.global.score_2 = 1;

        game.time.events.loop(2500, this.updateScore, this);
        

        score_here = 1;
        one_die = 0;
        two_die = 0;
        
         /*background*/
        this.bg = game.add.tileSprite(25,30,400,400,'bg');

        /*platform*/
        this.platforms = game.add.group();
        this.platforms.enableBody = true;
        this.platforms.createMultiple(8,'normal');
        this.platforms.createMultiple(3,'nails');
        this.platforms.createMultiple(3,'fake');
        this.platforms.createMultiple(3,'conveyorRight');
        this.platforms.createMultiple(3,'conveyorLeft');
        this.platforms.createMultiple(3,'trampoline');
        this.createPlatforms();
        game.time.events.loop(800,this.createPlatforms,this);
        game.time.events.loop(25000,this.addnails,this);

        normalsound = game.add.audio('normalsound');
        fakesound = game.add.audio('fakesound');
        jumpsound = game.add.audio('jumpsound');
        nailsound = game.add.audio('nailsound');
        conveyorsound = game.add.audio('conveyorsound');

        diesound = game.add.audio('diesound');

        /*ceiling*/
        ceiling = game.add.sprite(25,65,'ceiling');
        game.physics.arcade.enable(ceiling);
        ceiling.body.immovable = true;


        /*wall*/
        this.left_wall = game.add.tileSprite(25,30,18,400,'wall');
        game.physics.arcade.enable(this.left_wall);
        this.left_wall.body.immovable = true;

        this.right_wall = game.add.tileSprite(425,30,18,400,'wall');
        game.physics.arcade.enable(this.right_wall);
        this.right_wall.body.immovable = true;


        /*player*/
        this.player = game.add.sprite(200, 100, 'player'); // (x,y,name)
        game.physics.arcade.enable(this.player); //物理引擎
        this.player.anchor.setTo(0.5, 0.5);
        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8);
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        this.player.body.gravity.y = 500;

        /*player2*/
        this.player2 = game.add.sprite(250, 100, 'player2'); // (x,y,name)
        game.physics.arcade.enable(this.player2); //物理引擎
        this.player2.anchor.setTo(0.5, 0.5);
        this.player2.animations.add('leftwalk', [0, 1, 2, 3], 8);
        this.player2.animations.add('rightwalk', [9, 10, 11, 12], 8);
        this.player2.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player2.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player2.animations.add('fly', [36, 37, 38, 39], 12);
        this.player2.body.gravity.y = 500;

        /*border*/
        this.border = game.add.tileSprite(0,0,634,436,'border');

        /*life_title*/
        this.life_title_two = game.add.tileSprite(40,18,416,32,'life_title_two');

        /*life*/
        
        life = game.add.tileSprite(360,34,96,16,'life');
        life.frame = 12;

        life2 = game.add.tileSprite(40,34,96,16,'life');
        life2.frame = 12;
        

        /*score*/
        hundred = game.add.tileSprite(220,18,31,32,'number');
        hundred.frame = 0;

        ten = game.add.tileSprite(250,18,31,32,'number');
        ten.frame = 0;

        one = game.add.tileSprite(280,18,31,32,'number');
        one.frame = 1;
        
        /*record*/
        record_thousand = game.add.tileSprite(543,165,13,14,'record_score');
        record_thousand.frame = 0;

        record_hundred = game.add.tileSprite(556,165,13,14,'record_score');
        record_hundred.frame = 0;

        record_ten = game.add.tileSprite(569,165,13,14,'record_score');
        record_ten.frame = 0;

        record_one = game.add.tileSprite(582,165,13,14,'record_score');
        record_one.frame = 0;

        /*pause*/
        pause_button = game.add.image(game.width - 125, 300, 'pause_button');
        pause_button.inputEnabled = true;
        pause_button.events.onInputUp.add(function () 
        {
            game.paused = true;
            pause_label = game.add.sprite(170, 210, 'pause_label');
        });

        game.input.onDown.add(unpause, self);

        function unpause(event){
            if(game.paused)
            {
                    pause_label.destroy();
                    game.paused = false;
            }
        };

        abort_button = game.add.image(game.width - 125, 360, 'abort_button');
        abort_button.inputEnabled = true;
        abort_button.events.onInputUp.add(function () 
        {
            game.state.start('menu');
        });

        /*firebase*/
        var postsRef = firebase.database().ref('score').orderByChild('score').limitToFirst(1);

        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot){
                var childData = childSnapshot.val();
                record_score = childData.score * (-1);
                record_thousand.frame = parseInt(record_score / 1000);
                record_hundred.frame = parseInt(record_score / 100 - record_thousand.frame * 10);
                record_ten.frame  = parseInt(record_score / 10  - record_thousand.frame * 100 - record_hundred.frame * 10);
                record_one.frame = record_score % 10;
                })
               
            });


        unbeatableTime = 0;
        unbeatableTime_2 = 0;

        keyboard = game.input.keyboard.addKeys(
            {
                'left': Phaser.Keyboard.LEFT,
                'right' : Phaser.Keyboard.RIGHT,
                'a' : Phaser.Keyboard.A,
                'd' : Phaser.Keyboard.D
            });

    },

    addnails : function()
    {
        this.platforms.createMultiple(3,'nails');
    },

    update: function() {
        this.bg.tilePosition.y -= 1;
        this.left_wall.tilePosition.y -= 2;
        this.right_wall.tilePosition.y -= 2;
        this.movePlayer();
    },

    updateScore : function()
    {
        score_here += 1;
        hundred.frame = parseInt(score_here / 100);
        ten.frame = parseInt(score_here / 10 - hundred.frame * 10);
        one.frame = score_here % 10;
    },

   
    movePlayer: function() 
    {      
        game.physics.arcade.collide(this.player, [this.left_wall, this.right_wall]);
        game.physics.arcade.collide(this.player2,[this.left_wall,this.right_wall]);
        game.physics.arcade.collide(this.player2,this.platforms,this.Animation_2);
        game.physics.arcade.collide(this.player, this.platforms,this.Animation);
        game.physics.arcade.collide(this.player2,ceiling,this.checkTouchCeiling_2);
        game.physics.arcade.collide(this.player,ceiling,this.checkTouchCeiling);
        game.physics.arcade.collide(this.player,this.player2);
        this.checkGameover(this.player,this.player2);
        

        if (keyboard.left.isDown)
        {
            this.player.body.velocity.x = -250;
            this.player.animations.play('leftwalk');
        }
        else if(keyboard.right.isDown)
        {
            this.player.body.velocity.x = 250;
            this.player.animations.play('rightwalk');
        }
        else
        {
            this.player.body.velocity.x = 0;
        }

        if(keyboard.a.isDown)
        {
            this.player2.body.velocity.x = -250;
            this.player2.animations.play('leftwalk');
        }
        else if(keyboard.d.isDown)
        {
            this.player2.body.velocity.x = 250;
            this.player2.animations.play('rightwalk');
        }
        else
        {
            this.player2.body.velocity.x = 0;
        }
        this.setPlayerAnimate(this.player);
        this.setPlayerAnimate_2(this.player2);
        
    },

    createPlatforms: function()
    {
        var plarform;
        var x = Math.random()*(400 - 96 - 40) + 50;
        platform = this.platforms.getRandom();
        while(platform.alive) platform = this.platforms.getRandom();
        if(platform.key == "fake") platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        else if(platform.key == "nails") platform.body.setSize(96, 15, 0, 15);
        else if(platform.key == "conveyorLeft") 
        {
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        }
        else if(platform.key == "conveyorRight")
        {
            platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
            platform.play('scroll');
        }
        else if(platform.key == "trampoline") 
        {
            platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
            platform.frame = 3;
        }
        platform.reset(x,400);
        platform.body.velocity.y = -100;
        game.physics.arcade.enable(platform);
        platform.body.immovable = true;
        platform.body.checkCollision.up = true;
        platform.body.checkCollision.down = false;
        platform.body.checkCollision.right = false;
        platform.body.checkCollision.left = false;
        platform.checkWorldBounds = true;
        platform.outOfBoundsKill = true;
    },

    Animation : function(player, platform)
    {
        if(player.body.y + 32 > platform.body.y) return;

        if(platform.key == 'conveyorRight') {
            player.body.x += 2;
            if (player.touchOn !== platform) 
            {
                if(life.frame < 12) life.frame += 1;
                conveyorsound.play();
                player.touchOn = platform;
            }
        }
        if(platform.key == 'conveyorLeft') {
            player.body.x -= 2;
            if (player.touchOn !== platform) 
            {
                if(life.frame < 12) life.frame += 1;
                conveyorsound.play();
                player.touchOn = platform;
            }
        }
        if(platform.key == 'trampoline') {
            platform.animations.play('jump');
            player.body.velocity.y = -250;
            jumpsound.play();
            if(life.frame < 12) life.frame += 1;
            if (player.touchOn !== platform)
            {
                 player.touchOn = platform;
            }
        }
        if(platform.key == 'nails') {
            if (player.touchOn !== platform) {
                nailsound.play();
               if(life.frame >= 3) life.frame -= 3;
                else life.frame = 0;
                player.touchOn = platform;
                game.camera.flash(0xff0000, 100);
            }
        }
        if(platform.key == 'normal') {
            if (player.touchOn !== platform) 
            {
                normalsound.play();
                if(life.frame < 12) life.frame += 1;
                player.touchOn = platform;
            }
        }
        if(platform.key == 'fake') {
            if(player.touchOn !== platform) {
                platform.animations.play('turn');
                fakesound.play();
                if(life.frame < 12) life.frame += 1;
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
            }
        }
    },

    Animation_2 : function(player2, platform)
    {
        if(player2.body.y + 32 > platform.body.y) return;

        if(platform.key == 'conveyorRight') {
            player2.body.x += 2;
            if (player2.touchOn !== platform) 
            {
                if(life2.frame < 12) life2.frame += 1;
                conveyorsound.play();
                player2.touchOn = platform;
            }
        }
        if(platform.key == 'conveyorLeft') {
            player2.body.x -= 2;
            if (player2.touchOn !== platform) 
            {
                if(life2.frame < 12) life2.frame += 1;
                conveyorsound.play();
                player2.touchOn = platform;
            }
        }
        if(platform.key == 'trampoline') {
            platform.animations.play('jump');
            player2.body.velocity.y = -250;
            jumpsound.play();
            if(life2.frame < 12) life2.frame += 1;
            if (player2.touchOn !== platform)
            {
                 player2.touchOn = platform;
            }
        }
        if(platform.key == 'nails') {
            if (player2.touchOn !== platform) {
                nailsound.play();
               if(life2.frame >= 3) life2.frame -= 3;
                else life2.frame = 0;
                player2.touchOn = platform;
                game.camera.flash(0xff0000, 100);
            }
        }
        if(platform.key == 'normal') {
            if (player2.touchOn !== platform) 
            {
                normalsound.play();
                if(life2.frame < 12) life2.frame += 1;
                player2.touchOn = platform;
            }
        }
        if(platform.key == 'fake') {
            if(player2.touchOn !== platform) {
                platform.animations.play('turn');
                fakesound.play();
                if(life2.frame < 12) life2.frame += 1;
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player2.touchOn = platform;
            }
        }
    },

    setPlayerAnimate : function(player) {

        var x = player.body.velocity.x;
        var y = player.body.velocity.y;
    
        if (x < 0 && y > -100) {
            player.animations.play('flyleft');
        }
        if (x > 0 && y > -100) {
            player.animations.play('flyright');
        }
        if (x < 0 && y == -100) {
            player.animations.play('leftwalk');
        }
        if (x > 0 && y == -100) {
            player.animations.play('rightwalk');
        }
        if (x == 0 && y != -100) {
            player.animations.play('fly');
        }
        if (x == 0 && y == -100) {
          player.frame = 8;
        }
    },

    setPlayerAnimate_2 : function(player2) {

        var x = player2.body.velocity.x;
        var y = player2.body.velocity.y;
    
        if (x < 0 && y > -100) {
            player2.animations.play('flyleft');
        }
        if (x > 0 && y > -100) {
            player2.animations.play('flyright');
        }
        if (x < 0 && y == -100) {
            player2.animations.play('leftwalk');
        }
        if (x > 0 && y == -100) {
            player2.animations.play('rightwalk');
        }
        if (x == 0 && y != -100) {
            player2.animations.play('fly');
        }
        if (x == 0 && y == -100) {
          player2.frame = 8;
        }
    },

    checkTouchCeiling : function(player) {

        if(game.time.now > unbeatableTime) {
            console.log(1);
            if(life.frame >= 5) life.frame -= 5;
            else life.frame = 0;
            game.camera.flash(0xff0000, 100);
            unbeatableTime = game.time.now + 600;
        }
    },

    checkTouchCeiling_2 : function(player2) {

        if(game.time.now > unbeatableTime_2) {
            console.log(1);
            if(life2.frame >= 5) life2.frame -= 5;
            else life2.frame = 0;
            game.camera.flash(0xff0000, 100);
            unbeatableTime_2 = game.time.now + 600;
        }
    },
    
    checkGameover : function(player,player2)
    {
        if((life.frame == 0 || player.body.velocity.y > 500) && one_die == 0)
        {
            if(player.body.velocity.y > 500) diesound.play();
            one_die = 1;
            player.kill();
            gameover_1 = game.add.tileSprite(360,34,96,16,'gameover_0');
            game.global.score = score_here;
        }
        else if((life2.frame == 0 || player2.body.velocity.y > 500) && two_die == 0)
        {
            if(player.body.velocity.y > 500) diesound.play();
            two_die = 1;
            player2.kill();
            gameover_2 = game.add.tileSprite(40,34,96,16,'gameover_0');
            game.global.score_2 = score_here;
        }
        else if(one_die && two_die)
            game.state.start('over2');

    }
    
};



