var recordState = 
{
    preload : function(){
        game.load.bitmapFont('Book_Antiqua', 'font/font_0.png','font/font.xml',false);
    },

    create : function()
    {
        keyboard = game.input.keyboard.addKeys(
            {
                'up': Phaser.Keyboard.UP,
                'esc' : Phaser.Keyboard.ESC,
                'down' : Phaser.Keyboard.Down
            });
        
        var scoreboard = game.add.sprite(game.width / 2,20,'scoreboard');
        scoreboard.anchor.setTo(0.5,0);

        var name = game.add.sprite(game.width / 2 - 100,70,'name');
        name.anchor.setTo(0.5,0);

        var score = game.add.sprite(game.width / 2 + 100,80,'score');
        score.anchor.setTo(0.5,0);

        var crown = game.add.sprite(game.width / 2 - 230,160,'crown');
        crown.anchor.setTo(0.5,0.5);

        var number_two = game.add.sprite(game.width / 2 - 230,210,'2');
        number_two.anchor.setTo(0.5,0.5);

        var number_three = game.add.sprite(game.width / 2 - 230,250,'3');
        number_three.anchor.setTo(0.5,0.5);

        var number_four = game.add.sprite(game.width / 2 - 230,290,'4');
        number_four.anchor.setTo(0.5,0.5);

        var number_five = game.add.sprite(game.width / 2 - 230,330,'5');
        number_five.anchor.setTo(0.5,0.5);
        

        var esc = game.add.sprite(0,400,'esc');
        esc.anchor.setTo(0,0.5);

        /*database*/

        var postsRef = firebase.database().ref('score').orderByChild('score').limitToFirst(5);
        var total_post = [];
        var total_po = [];

        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot){
                var childData = childSnapshot.val();
                total_post[total_post.length] = childData.username;
                total_po[total_po.length] = childData.score * (-1);
                });

                for(var i = 0; i < total_post.length ; i++)
                {
                    var data = game.add.bitmapText(game.width / 2 - 100,170 + i * 40,'Book_Antiqua',total_post[i],40);
                    data.anchor.setTo(0.5,0.5);
                    var data_score = game.add.bitmapText(game.width / 2 + 150,170 + i * 40,'Book_Antiqua',total_po[i],40);
                    data_score.anchor.setTo(1,0.5);
                }
            })
            .catch(e => console.log(e.message));
        inst = game.add.audio('inst');

    },

    update : function()
    {
        if(keyboard.esc.isDown)
        {
            inst.play();
            game.state.start('menu');
        }
    }
};