var bootState = {
    preload : function()
    {
        game.load.spritesheet('loading_bar','img/life.png',96,16); // loading bar
        game.load.image('load','img/load.png');

        
    },

    create : function()
    {
        game.stage.backgroundColor = '#3498db';
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        game.state.start('load');
    }
};