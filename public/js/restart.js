var restartState = 
{
    preload : function()
    {
        var start = game.add.sprite(game.width / 2 ,150,'up');
        start.anchor.setTo(0.5,0.5);

        var down = game.add.sprite(game.width / 2,210,'down');
        down.anchor.setTo(0.5,0.5);

        var esc = game.add.sprite(game.width / 2 ,270,'esc_restart');
        esc.anchor.setTo(0.5,0.5);
    },

    create : function()
    {
        keyboard = game.input.keyboard.addKeys(
            {
                'up': Phaser.Keyboard.UP,
                'esc' : Phaser.Keyboard.ESC,
                'down' : Phaser.Keyboard.Down
            });
        inst = game.add.audio('inst');
    },

    update : function()
    {
        if(keyboard.up.isDown)
        {
            inst.play();
            game.state.start('main');
        }
        if(keyboard.esc.isDown)
        {
            inst.play();
            game.state.start('menu');
        }
        if(keyboard.down.isDown)
        {
            inst,play();
            game.state.start('2players');
        }
    }
}