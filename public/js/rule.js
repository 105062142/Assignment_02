var ruleState = 
{
    preload : function()
    {

    },

    create : function()
    {
        keyboard = game.input.keyboard.addKeys(
            {
                'up': Phaser.Keyboard.UP,
                'esc' : Phaser.Keyboard.ESC,
                'down' : Phaser.Keyboard.DOWN,
            });
        
            inst = game.add.audio('inst');

        var rule = game.add.sprite(game.width / 2,20,'rule');
        rule.anchor.setTo(0.5,0);
        
        var one = game.add.sprite(game.width / 2 - 150,90,'1p');
        one.anchor.setTo(0.5,0.5);

        var one_left = game.add.sprite(game.width / 2 + 20,140,'1p_rule_left');
        one_left.anchor.setTo(0.5,0.5);

        var one_right = game.add.sprite(game.width / 2 + 20,170,'1p_rule_right');
        one_right.anchor.setTo(0.5,0.5);

        var two = game.add.sprite(game.width / 2 - 165,220,'2p');
        two.anchor.setTo(0.5,0.5);

        var two_left = game.add.sprite(game.width / 2 + 10,270,'2p_rule_left');
        two_left.anchor.setTo(0.5,0.5);

        var two_right = game.add.sprite(game.width / 2 + 20,300,'2p_rule_right');
        two_right.anchor.setTo(0.5,0.5);

        var esc = game.add.sprite(0,350,'esc');
        esc.anchor.setTo(0,0.5);

        var up = game.add.sprite(game.width,350,'up_record');
        up.anchor.setTo(1,0.5);

        var down = game.add.sprite(game.width,400,'down_record');
        down.anchor.setTo(1,0.5);

        this.player = game.add.sprite(game.width / 2 - 100,90, 'player'); // (x,y,name)
        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8,true);
        this.player.animations.play('leftwalk');
        this.player.anchor.setTo(0.5,0.5);

        this.player2 = game.add.sprite(game.width / 2 - 100,220,'player2');
        this.player2.animations.add('rightwalk',[9, 10, 11, 12], 8,true);
        this.player2.animations.play('rightwalk');
        this.player2.anchor.setTo(0.5,0.5);
    },

    update : function()
    {
        if(keyboard.up.isDown)
        {
            inst.play();
            game.state.start('main');
        }
        if(keyboard.down.isDown)
        {
            inst.play();
            game.state.start('2players');
        }
        if(keyboard.esc.isDown)
        {
            inst.play();
            game.state.start('menu');
        }
    }
};
