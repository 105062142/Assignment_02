var overState = {
    preload : function()
    {
        game.stage.backgroundColor = '#000000';
        var gameover = game.add.sprite(game.width / 2,130,'gameover');
        gameover.anchor.setTo(0.5,0.5);

        var enter_name = game.add.sprite(game.width / 2,200,'enter_name');
        enter_name.anchor.setTo(0.5,0.5);

        var english_label = game.add.sprite(game.width / 2,270,'english_label');
        english_label.anchor.setTo(0.5,0.5);
    },

    create : function()
    {
        keyboard = game.input.keyboard.addKeys(
            {
                'enter': Phaser.Keyboard.ENTER,
                'esc': Phaser.Keyboard.ESC
        });
        game.add.plugin(Fabrique.Plugins.InputField); 
        input = game.add.inputField(240, 220,{
            font: '18px Arial',
            fill: '#212121',
            fontWeight: 'bold',
            width: 150,
            padding: 8,
            borderWidth: 1,
            borderColor: '#000',
            borderRadius: 6,
            placeHolder: 'User name',
            type: Fabrique.InputType.text
        }); 
        inst = game.add.audio('inst');  
    },

    update : function()
    {
        if(keyboard.enter.isDown)
        {
            if (input.value != "") {
                var ref = firebase.database().ref('score');
                var data = {
                    username : input.value,
                    score: game.global.score * (-1)
                };
                ref.push(data);
                game.state.start('restart');
            }
        }
        if(keyboard.esc.isDown)
        {
            inst.play();
            game.state.start('menu');
        }
    }

};