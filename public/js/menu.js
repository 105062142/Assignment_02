var menuState = 
{
    preload : function(){},

    create : function()
    {
        game.stage.backgroundColor = '#000000';
        var title = game.add.sprite(game.width / 2,70,'title');
        title.anchor.setTo(0.5,0.5);

        var start = game.add.sprite(game.width / 2 ,150,'up');
        start.anchor.setTo(0.5,0.5);

        var down = game.add.sprite(game.width / 2,210,'down');
        down.anchor.setTo(0.5,0.5);

        var record = game.add.sprite(game.width / 2 ,270,'s');
        record.anchor.setTo(0.5,0.5);

        var rule = game.add.sprite(game.width / 2,330,'r');
        rule.anchor.setTo(0.5,0.5);


        keyboard = game.input.keyboard.addKeys(
            {
                'up': Phaser.Keyboard.UP,
                's' : Phaser.Keyboard.S,
                'down' : Phaser.Keyboard.DOWN,
                'r' : Phaser.Keyboard.R
            });

        inst = game.add.audio('inst');

    },
    
    update : function()
    {
        if(keyboard.up.isDown)
        {
            inst.play();
            game.state.start('main');
        }
        if(keyboard.s.isDown)
        {
            inst.play();
            game.state.start('record');
        }
        if(keyboard.down.isDown)
        {
            inst.play();
            game.state.start('2players');
        }
        if(keyboard.r.isDown)
        {
            inst.play();
            game.state.start('rule');
        }
    }
};