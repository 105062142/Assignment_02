var over2State = {
    preload : function()
    {
        game.stage.backgroundColor = '#000000';

        var enter_name = game.add.sprite(game.width / 2,200,'enter_name');
        enter_name.anchor.setTo(0.5,0.5);

        var one = game.add.sprite(game.width / 2 -10,250,'1p');
        one.anchor.setTo(1,1);

        var two = game.add.sprite(game.width / 2-30,330,'2p');
        two.anchor.setTo(1,1);

        var english_label = game.add.sprite(game.width / 2,350,'english_label');
        english_label.anchor.setTo(0.5,0.5);
        
    },

    create : function()
    {
        keyboard = game.input.keyboard.addKeys(
            {
                'enter': Phaser.Keyboard.ENTER,
                'esc': Phaser.Keyboard.ESC
        });
        game.add.plugin(Fabrique.Plugins.InputField); 
        input1 = game.add.inputField(240, 220,{
            font: '18px Arial',
            fill: '#212121',
            fontWeight: 'bold',
            width: 150,
            padding: 8,
            borderWidth: 1,
            borderColor: '#000',
            borderRadius: 6,
            placeHolder: 'User name',
            type: Fabrique.InputType.text
        }); 

        input2 = game.add.inputField(240, 300,{
            font: '18px Arial',
            fill: '#212121',
            fontWeight: 'bold',
            width: 150,
            padding: 8,
            borderWidth: 1,
            borderColor: '#000',
            borderRadius: 6,
            placeHolder: 'User name',
            type: Fabrique.InputType.text
        });
        inst = game.add.audio('inst'); 
        
        if(game.global.score > game.global.score_2)
        {
            var player1win = game.add.sprite(game.width / 2,130,'player1win');
            player1win.anchor.setTo(0.5,0.5);
        }
        else if(game.global.score_2 > game.global.score)
        {
            var player2win = game.add.sprite(game.width / 2,130,'player2win');
            player2win.anchor.setTo(0.5,0.5);
        }
        else
        {
            var tie = game.add.sprite(game.width / 2,130,'tie');
            tie.anchor.setTo(0.5,0.5);
        }
    },

    update : function()
    {
        if(keyboard.enter.isDown)
        {
            if (input1.value != "" && input2.value != "") {
                var ref = firebase.database().ref('score');
                var data = {
                    username : input1.value,
                    score: game.global.score * (-1)
                };
                var data2 = {
                    username : input2.value,
                    score : game.global.score_2 * (-1)
                }
                ref.push(data);
                ref.push(data2);
                game.state.start('restart');
            }
        }
        if(keyboard.esc.isDown)
        {
            inst.play();
            game.state.start('menu');
        }
    }

};